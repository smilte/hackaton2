package com.example.hackaton2.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TradeAdvisor {
    
    private String ticker;
    private Double lastClose;
    private Double upperBand;
    private Double lowerBand;
    private String advice;

    public TradeAdvisor() {}

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public Double getLastClose() {
        return lastClose;
    }

    public void setLastClose(Double lastClose) {
        this.lastClose = lastClose;
    }

    public Double getUpperBand() {
        return upperBand;
    }

    public void setUpperBand(Double upperBand) {
        this.upperBand = upperBand;
    }

    public Double getLowerBand() {
        return lowerBand;
    }

    public void setLowerBand(Double lowerBand) {
        this.lowerBand = lowerBand;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    @Override
    public String toString() {
        return "TradeAdvisor{" +
                "ticker='" + ticker + '\'' +
                ", lastClose=" + lastClose +
                ", upperBand=" + upperBand +
                ", lowerBand=" + lowerBand +
                ", advice='" + advice + '\'' +
                '}';
    }
}
