package com.example.hackaton2.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceFeed {

    private String ticker;
    @JsonProperty("price_data")
    private List<PriceDataForPriceFeed> price_data;

    public PriceFeed() {}

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public List<PriceDataForPriceFeed> getPrice_data() {
        return price_data;
    }

    public void setPrice_data(List<PriceDataForPriceFeed> price_data) {
        this.price_data = price_data;
    }

    @Override
    public String toString() {
        return "PriceFeed{" +
                "ticker='" + ticker + '\'' +
                ", price_data=" + price_data +
                '}';
    }
}
