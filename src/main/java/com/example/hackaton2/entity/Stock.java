package com.example.hackaton2.entity;

public class Stock {

    private String ticker;
    private String company;
    private double price;
    private String industry;
    private String advice;

    public Stock() {}

    public Stock(String ticker, String company, double price, String industry) {
        this.ticker = ticker;
        this.company = company;
        this.price = price;
        this.industry = industry;
        this.advice = "N/A";
    }

    public Stock(String ticker, String company, double price, String industry, String advice) {
        this.ticker = ticker;
        this.company = company;
        this.price = price;
        this.industry = industry;
        this.advice = advice;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        company = company;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "ticker='" + ticker + '\'' +
                ", company='" + company + '\'' +
                ", price=" + price +
                ", industry='" + industry + '\'' +
                ", advice='" + advice + '\'' +
                '}';
    }
}
