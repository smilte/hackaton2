package com.example.hackaton2.entity;

/**
 * StockTrades is the main entity we will be using
 * @author Ioana Roceanu, Osato Osajie, Smilte Petronyte, Tara Ruxton
 */

import javax.persistence.*;
import java.util.Date;

@Entity(name="stocktrades")
public class StockTrades {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String stockTicker;
    private double price;
    private int volume;
    private String buyOrSell;
    private int statusCode;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdTimestamp = new Date(System.currentTimeMillis());

    public Long getId() {
        return id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public double getPrice() {
        return price;
    }

    public int getVolume() {
        return volume;
    }

    public String getBuyOrSell() {
        return buyOrSell;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public Date getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public void setBuyOrSell(String buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setCreatedTimestamp(Date createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    @Override
    public String toString() {
        return "StockTrades{" +
                "id=" + id +
                ", stockTicker='" + stockTicker + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                ", buyOrSell='" + buyOrSell + '\'' +
                ", statusCode=" + statusCode +
                ", createdTimestamp=" + createdTimestamp +
                '}';
    }

}
