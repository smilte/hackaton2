package com.example.hackaton2.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceDataForPriceFeed {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date name;
    private Double value;

    public Date getName() {
        return name;
    }

    public void setName(Date name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "PriceDataForPriceFeed{" +
                "name=" + name +
                ", value=" + value +
                '}';
    }
}
