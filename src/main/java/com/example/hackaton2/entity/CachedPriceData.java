package com.example.hackaton2.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CachedPriceData {

    private String ticker;
    @JsonProperty("price_data")
    private PriceDataForCached price_data;

    public CachedPriceData() { }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public PriceDataForCached getPrice_data() {
        return price_data;
    }

    public void setPrice_data(PriceDataForCached price_data) {
        this.price_data = price_data;
    }

    @Override
    public String toString() {
        return "CachedPriceData{" +
                "ticker='" + ticker + '\'' +
                ", price_data=" + price_data +
                '}';
    }
}
