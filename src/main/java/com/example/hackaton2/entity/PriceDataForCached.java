package com.example.hackaton2.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceDataForCached {

    private List<Double> open;
    private List<Double> high;
    private List<Integer> volume;
    private List<Double> low;
    private List<Double> close;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private List<Date> timestamp;

    public PriceDataForCached() {}

    public List<Double> getOpen() {
        return open;
    }

    public void setOpen(List<Double> open) {
        this.open = open;
    }

    public List<Double> getHigh() {
        return high;
    }

    public void setHigh(List<Double> high) {
        this.high = high;
    }

    public List<Integer> getVolume() {
        return volume;
    }

    public void setVolume(List<Integer> volume) {
        this.volume = volume;
    }

    public List<Double> getLow() {
        return low;
    }

    public void setLow(List<Double> low) {
        this.low = low;
    }

    public List<Double> getClose() {
        return close;
    }

    public void setClose(List<Double> close) {
        this.close = close;
    }

    public List<Date> getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(List<Date> timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "PriceData{" +
                "open=" + open +
                ", high=" + high +
                ", volume=" + volume +
                ", low=" + low +
                ", close=" + close +
                ", timestamp=" + timestamp +
                '}';
    }
}
