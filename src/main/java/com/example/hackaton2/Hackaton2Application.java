package com.example.hackaton2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hackaton2Application {

    public static void main(String[] args) {
        SpringApplication.run(Hackaton2Application.class, args);
    }
}
