package com.example.hackaton2.repository;

import com.example.hackaton2.entity.StockTrades;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StockTradeRepository extends JpaRepository<StockTrades, Long> {

    List<StockTrades> findTradesByStockTicker(String stockTicker);

    List<StockTrades> findTradesByStatusCode(int statusCode);

}
