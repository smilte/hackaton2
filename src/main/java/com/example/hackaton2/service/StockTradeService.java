package com.example.hackaton2.service;

import com.example.hackaton2.controller.Action;
import com.example.hackaton2.controller.StockTradesController;
import com.example.hackaton2.entity.PriceDataForCached;
import com.example.hackaton2.entity.PriceFeed;
import com.example.hackaton2.entity.CachedPriceData;
import com.example.hackaton2.entity.StockTrades;
import com.example.hackaton2.repository.StockTradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownContentTypeException;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class StockTradeService {

    @Autowired
    StockTradeRepository stockTradeRepository;
    @Autowired
    private RestTemplate restTemplate;

    public List<StockTrades> findAll(){
        return stockTradeRepository.findAll();
    }

    public StockTrades findById(long id){
        return stockTradeRepository.findById(id).get();
    }

    public List<String> getOwnedTickers(){
        List<String> ownedTickers = new ArrayList<>();
        List<String> cachedTickers = new ArrayList<>(Arrays. asList("AMZN", "AAPL", "C", "FB", "GOOGL", "TSLA"));
        for(String ticker: cachedTickers){
            if(countBasketOf(ticker) > 0){
                ownedTickers.add(ticker);
            }
        }
        return ownedTickers;
    }

    /*returns an array [number of shares sold/bought, total revenue/price]*/
    public List<Double> valueCountPosition(String stockTicker, Action action){
        List<StockTrades> trades = stockTradeRepository.findTradesByStockTicker(stockTicker);
        System.out.println(action.toString());
        double count = 0;
        double value = 0;
        for(StockTrades trade : trades){
            String tradeAction = trade.getBuyOrSell();
            if(tradeAction.equals(action.toString())){
                int volume = trade.getVolume();
                count += volume;
                value += volume* trade.getPrice();
            }
        }
        List<Double> countAndValue= new ArrayList<Double>();
        countAndValue.add(count);
        countAndValue.add(value);
        return countAndValue;
    }

    /*return price from latest order -DUMMY*/
    public Double getCurrentPrice(String stockTicker){
        List<StockTrades> trades = stockTradeRepository.findTradesByStockTicker(stockTicker);
        StockTrades lastTrade = trades.get(trades.size() - 1);
        return lastTrade.getPrice();
    }

    public int getIndexOfDate(Date ourDate, Date firstDate){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(ourDate);
        calendar.set(Calendar.SECOND, 0);
        int minutes = calendar.get(Calendar.MINUTE);
        calendar.add(Calendar.MINUTE, minutes == 0 ? -5 : -(minutes % 5));
        ourDate = calendar.getTime();
        // todays indexed from 312-390
        // 79 intervals per day

        long diff = ourDate.getTime() - firstDate.getTime();

        TimeUnit time = TimeUnit.MINUTES;
        long diffrence = time.convert(diff, TimeUnit.MILLISECONDS);
        diffrence = diffrence/5;
        return (int)(312+diffrence);
    }

    public Double getPriceByTickerYesterday(String ticker){

        List<String> cachedTickers = new ArrayList<>(Arrays. asList("AMZN", "AAPL", "C", "FB", "GOOGL", "TSLA"));
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 9);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.SECOND, 0);
        Date openingTime = calendar.getTime();
        calendar.set(Calendar.HOUR, 16);
        calendar.set(Calendar.MINUTE, 0);
        Date closingTime = calendar.getTime();

        try{
            if (cachedTickers.contains(ticker) && (currentTime.after(openingTime) || currentTime.equals(openingTime))) {
                CachedPriceData cachedPriceData = restTemplate.getForObject("https://c4rm9elh30.execute-api.us-east-1.amazonaws.com/default/cachedPriceData?ticker=" + ticker,
                        CachedPriceData.class);
                if (cachedPriceData.getTicker() != null && cachedPriceData.getTicker().equals(ticker)) {
                    if(currentTime.before(closingTime)) {
                        int index = getIndexOfDate(currentTime, openingTime) - 78;
                        return cachedPriceData.getPrice_data().getClose().get(index);
                    }
                    else {
                        List<Double> closes = cachedPriceData.getPrice_data().getClose();
                        return closes.get(closes.size() - 78);
                    }
                }
            }
            else {
                PriceFeed priceFeed = restTemplate
                        .getForObject("https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker=" + ticker + "&num_days=" + 2,
                                PriceFeed.class);
                if (priceFeed.getTicker() != null && priceFeed.getTicker().equals(ticker)) {
                    return priceFeed.getPrice_data().get(0).getValue();
                }
            }
        }catch(NoSuchElementException | UnknownContentTypeException | HttpServerErrorException ex){

            return 0.0;
        }

        return 0.0;
    }

    public Double getPriceByTicker(String ticker){

        List<String> cachedTickers = new ArrayList<>(Arrays. asList("AMZN", "AAPL", "C", "FB", "GOOGL", "TSLA"));
        Date currentTime = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, 9);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.SECOND, 0);
        Date openingTime = calendar.getTime();
        calendar.set(Calendar.HOUR, 16);
        calendar.set(Calendar.MINUTE, 0);
        Date closingTime = calendar.getTime();

        try{
            if (cachedTickers.contains(ticker) && (currentTime.after(openingTime) || currentTime.equals(openingTime))) {
                CachedPriceData cachedPriceData = restTemplate.getForObject("https://c4rm9elh30.execute-api.us-east-1.amazonaws.com/default/cachedPriceData?ticker=" + ticker,
                        CachedPriceData.class);
                if (cachedPriceData.getTicker() != null && cachedPriceData.getTicker().equals(ticker)) {
                    if(currentTime.before(closingTime)) {
                        int index = getIndexOfDate(currentTime, openingTime);
                        return cachedPriceData.getPrice_data().getClose().get(index);
                    }
                    else {
                        List<Double> closes = cachedPriceData.getPrice_data().getClose();
                        return closes.get(closes.size() - 1);
                    }
                }
            }
            else {
                PriceFeed priceFeed = restTemplate
                        .getForObject("https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker=" + ticker + "&num_days=" + 1,
                                PriceFeed.class);
                if (priceFeed.getTicker() != null && priceFeed.getTicker().equals(ticker)) {
                        return priceFeed.getPrice_data().get(0).getValue();
                }
            }
        }catch(NoSuchElementException | UnknownContentTypeException | HttpServerErrorException ex){

            return 0.0;
        }

        return 0.0;
    }

    public Double pnlByTicker(String stockTicker) {
        List<Double> countAndValueBUY = valueCountPosition(stockTicker, Action.BUY);
        List<Double> countAndValueSELL = valueCountPosition(stockTicker, Action.SELL);
        Double overallCost = countAndValueBUY.get(1) - countAndValueSELL.get(1);
        int stash = countBasketOf(stockTicker);
        if(overallCost == 0){
            return 0.0;
        }
        double pnl = (getPriceByTicker(stockTicker)*stash)/overallCost;
        return (pnl - 1)*100;
    }


    public Map<String, Double> stocksAndPnl(List<String> owned){

        Map<String,Double> map= new HashMap<>();
        for(String stock : owned){
            map.put(stock, Math.round((pnlByTicker(stock))*100)/100.0);
//            map.put(stock,1000.0);
        }
        return map;

    }

    /*how much you received when selling positions minus how much you paid for positions */
    public Double aggregateCostOf(String stockTicker){
        List<StockTrades> trades = stockTradeRepository.findTradesByStockTicker(stockTicker);
        double costBought = 0;
        double revenueSold = 0;
        for(StockTrades trade : trades){
            String action = trade.getBuyOrSell();
            double price = trade.getPrice();
            int volume = trade.getVolume();
            if(action.equals(Action.BUY.toString())){
                costBought += volume*price;
            } else if(action.equals(Action.SELL.toString())){
                revenueSold += volume*price;
            }
        }
        return (Double)(revenueSold-costBought);
    }

    public Integer countBasketOf(String stockTicker){
        List<StockTrades> trades = stockTradeRepository.findTradesByStockTicker(stockTicker);
        int noBought = 0;
        int noSold = 0;
        for(StockTrades trade : trades){
            String action = trade.getBuyOrSell();
            int volume = trade.getVolume();
            if(action.equals(Action.BUY.toString())){
                noBought += volume;
            } else if(action.equals(Action.SELL.toString())){
                noSold += volume;
            }
        }
        return (Integer)(noBought-noSold);

    }

    public Map<String, Integer> countTotalSharesOwned(List<String> owned){

        Map<String,Integer> map= new HashMap<>();
        for(String stock : owned){
            map.put(stock,countBasketOf(stock));

        }
        return map;

    }

    public Map<String, Double> currentValueOfSharesInPortfolio(List<String> owned){

        Map<String,Double> map= new HashMap<>();


        for(String stock : owned){
            int numOfShares=countBasketOf(stock);
            double currentPrice= getPriceByTicker(stock);
            double currentVal= numOfShares*currentPrice;
            currentVal= Math.round(currentVal*100)/100.0;

            map.put(stock,currentVal);

        }
        return map;

    }



    public List<StockTrades> findByStockTicker(String stockTicker) {
        return stockTradeRepository.findTradesByStockTicker(stockTicker);
    }

    public List<StockTrades> findByStatusCode(int statusCode){
        return stockTradeRepository.findTradesByStatusCode(statusCode);
    }

    public StockTrades save(StockTrades stockTrades){
        return stockTradeRepository.save(stockTrades);
    }

    public StockTrades update(StockTrades stockTrades) {
        stockTradeRepository.findById(stockTrades.getId()).get();

        return stockTradeRepository.save(stockTrades);
    }

    public void delete(long id) {
        stockTradeRepository.deleteById(id);
    }

}
