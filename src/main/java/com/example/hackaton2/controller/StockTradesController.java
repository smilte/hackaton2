package com.example.hackaton2.controller;

import com.example.hackaton2.entity.*;
import com.example.hackaton2.service.StockTradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownContentTypeException;

import java.util.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/stocktrades")
public class StockTradesController {

    @Autowired
    private StockTradeService stockTradeService;
    @Autowired
    private RestTemplate restTemplate;
    private final List<String> CACHED_PRICE_DATA_TICKERS = new ArrayList<>(Arrays. asList("AMZN", "AAPL", "C", "FB", "GOOGL", "TSLA"));

    ArrayList<Integer> statusCodes = new ArrayList<>();

    public StockTradesController(){
        this.statusCodes.add(0);
        this.statusCodes.add(1);
        this.statusCodes.add(2);
        this.statusCodes.add(3);
    }

    @GetMapping
    public List<StockTrades> findAll(){
        return stockTradeService.findAll();
    }

    @GetMapping("/countBasketOf/{stockTicker}")
    public ResponseEntity<Integer> countBasketOf(@PathVariable String stockTicker){
        try{
            return new ResponseEntity<Integer>(stockTradeService.countBasketOf(stockTicker), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/aggregatedCost/{stockTicker}")
    public ResponseEntity<Double> aggregateCostOf(@PathVariable String stockTicker){
        try{
            return new ResponseEntity<Double>(stockTradeService.aggregateCostOf(stockTicker), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/pnl/{stockTicker}")
    public ResponseEntity<Double> pnlByTicker(@PathVariable String stockTicker){
        try{
            return new ResponseEntity<Double>(stockTradeService.pnlByTicker(stockTicker), HttpStatus.OK);
        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    //getOwnedTickers
    @GetMapping("/getOwnedTickers")
    public ResponseEntity<List<String>> getOwnedTickers(){
        try{
            return new ResponseEntity<List<String>>(stockTradeService.getOwnedTickers(), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/{id}")
    public ResponseEntity<StockTrades> findById(@PathVariable long id){
        try{
            return new ResponseEntity<StockTrades>(stockTradeService.findById(id), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping("/findByStockTicker/{stockTicker}")
    public ResponseEntity<List<StockTrades>> findByTicker(@PathVariable String stockTicker){
        try{
            return new ResponseEntity<List<StockTrades>>(stockTradeService.findByStockTicker(stockTicker), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }



    @GetMapping("/findByStatusCode/{statusCode}")
    public ResponseEntity<List<StockTrades>> findByStatusCode(@PathVariable int statusCode){
        try{
            if(0 <= statusCode && statusCode <= 3){
                return new ResponseEntity<>(stockTradeService.findByStatusCode(statusCode), HttpStatus.OK);
            } else {
                return new ResponseEntity("Not a valid status code",HttpStatus.NOT_ACCEPTABLE);
            }

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    public RequestValidity checkRequestValidity(StockTrades stockTrades){
        if( !(stockTrades.getBuyOrSell().equals(Action.BUY.toString()) ||
                stockTrades.getBuyOrSell().equals(Action.SELL.toString()) ) ){
            return RequestValidity.BUYORSELL_ERR;
        }
        if(stockTrades.getBuyOrSell().equals(Action.SELL.toString()) &&
                stockTradeService.countBasketOf(stockTrades.getStockTicker()) <  stockTrades.getVolume()){
            return RequestValidity.EXCEEDSBALANCE;
        }
        if(stockTrades.getPrice() < 0){
            return RequestValidity.PRICE_ERR;
        }
        if(stockTrades.getVolume() <= 0){
            return RequestValidity.VOLUME_ERR;
        }
        if(stockTrades.getStockTicker() == null ||
                stockTrades.getStockTicker().trim().isEmpty() ){
            return RequestValidity.STOCKTICKER_ERR;
        }
        if(stockTrades.getCreatedTimestamp() == null ){
            return RequestValidity.TIMESTAMP_ERR;
        }
        return RequestValidity.VALID;
    }

    @PostMapping
    public ResponseEntity<StockTrades> create(@RequestBody StockTrades stockTrades){
        RequestValidity validity = checkRequestValidity(stockTrades);
        switch (validity){
            case BUYORSELL_ERR:
                return new ResponseEntity("Action must be either BUY or SELL", HttpStatus.NOT_ACCEPTABLE);
            case EXCEEDSBALANCE:
                return new ResponseEntity("Balance exceeded, try selling less", HttpStatus.NOT_ACCEPTABLE);
            case PRICE_ERR:
                return new ResponseEntity("Price should not be negative",HttpStatus.NOT_ACCEPTABLE);
            case VOLUME_ERR:
                return new ResponseEntity("Volume must be a positive number", HttpStatus.NOT_ACCEPTABLE);
            case STOCKTICKER_ERR:
                return new ResponseEntity("Ticker must not be an empty string", HttpStatus.NOT_ACCEPTABLE);
            case TIMESTAMP_ERR:
                return new ResponseEntity("You cannot add a null timestamp", HttpStatus.NOT_ACCEPTABLE);
            default: return new ResponseEntity<StockTrades>(stockTradeService.save(stockTrades), HttpStatus.CREATED);
        }
    }

    @PutMapping
    public ResponseEntity<StockTrades> update(@RequestBody StockTrades stockTrades) {
        try {
            RequestValidity validity = checkRequestValidity(stockTrades);
            switch (validity){
                case BUYORSELL_ERR:
                    return new ResponseEntity("Action must be either BUY or SELL", HttpStatus.NOT_ACCEPTABLE);
                case PRICE_ERR:
                    return new ResponseEntity("Price should not be negative",HttpStatus.NOT_ACCEPTABLE);
                case VOLUME_ERR:
                    return new ResponseEntity("Volume must be a positive number", HttpStatus.NOT_ACCEPTABLE);
                case STOCKTICKER_ERR:
                    return new ResponseEntity("Ticker must not be an empty string", HttpStatus.NOT_ACCEPTABLE);
                case TIMESTAMP_ERR:
                    return new ResponseEntity("You cannot add a null timestamp", HttpStatus.NOT_ACCEPTABLE);
                default: return new ResponseEntity<StockTrades>(stockTradeService.update(stockTrades), HttpStatus.OK);
            }
        } catch(NoSuchElementException ex) {
            return new ResponseEntity<StockTrades>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        try {
            stockTradeService.delete(id);
        } catch(EmptyResultDataAccessException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    // Only works for the following tickers: 'AMZN', 'AAPL', 'C', 'FB', 'GOOGL', 'TSLA'
    @GetMapping("/getCachedPriceData/{ticker}")
    public ResponseEntity<Double> getCachedPriceData(@PathVariable String ticker) {

        try{

            return new ResponseEntity<>(stockTradeService.getPriceByTicker(ticker), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);}

    }

    //getPriceByTickerYesterday
    @GetMapping("/getPriceYesterday/{ticker}")
    public ResponseEntity<List<Double>> getPriceYesterday(@PathVariable String ticker) {
        try{
            List<Double> list = new ArrayList<>();
            list.add(stockTradeService.getPriceByTickerYesterday(ticker));
            list.add(stockTradeService.getPriceByTicker(ticker));
            return new ResponseEntity<>(list, HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);}

    }


    @GetMapping("/getStocksAndPnl/{list}")
    public ResponseEntity<Map<String, Double>> getPnlList(@PathVariable List<String> list) {

        try{

            return new ResponseEntity<>(stockTradeService.stocksAndPnl(list), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);}
    }




    @GetMapping("/getCurrentValueOfShares/{list}")
    public ResponseEntity<Map<String, Double>> getCurrentValueOfShares(@PathVariable List<String> list) {

        try{

            return new ResponseEntity<>(stockTradeService.currentValueOfSharesInPortfolio(list), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);}
    }




    @GetMapping("/totalSharesOwned/{list}")
    public ResponseEntity<Map<String, Integer>> getTotalSharesOwned(@PathVariable List<String> list) {

        try{

            return new ResponseEntity<>(stockTradeService.countTotalSharesOwned(list), HttpStatus.OK);

        }catch(NoSuchElementException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);}
    }

    @GetMapping("/getStockByTicker/{stockTicker}")
    public ResponseEntity<Stock> getStockByTicker(@PathVariable String stockTicker) {

        Ticker[] tickers = getTickerList().getBody();
        for(Ticker t: tickers) {
            if(t.getSymbol().equals(stockTicker)){
                System.out.println("Ticker found: ");
                Double price = getCachedPriceData(stockTicker).getBody();
                TradeAdvisor tradeAdvisor = getTradeAdvisor(stockTicker).getBody();
                Stock stock = new Stock(stockTicker, t.getName(), price, t.getIndustry());
                if (tradeAdvisor.getTicker().equals(stockTicker)){
                   stock = new Stock(stockTicker, t.getName(), price, t.getIndustry(), tradeAdvisor.getAdvice());
                }
                System.out.println(stock);
                return new ResponseEntity<Stock>(stock, HttpStatus.OK);
            }
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/getTickerList")
    public ResponseEntity<Ticker[]> getTickerList() {

        try{
            Ticker[] tickers =  restTemplate.getForObject("https://v588nmxc10.execute-api.us-east-1.amazonaws.com/default/tickerList", Ticker[].class);
            return new ResponseEntity<Ticker[]>(tickers, HttpStatus.OK);
        } catch (HttpServerErrorException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getPriceFeed/ticker={ticker}&num_days={numberOfDays)")
    public ResponseEntity<PriceFeed> getPriceFeed(@PathVariable("ticker") String ticker, @RequestParam("numberOfDays") int numberOfDays) {

        try{
            PriceFeed priceFeed = restTemplate
                    .getForObject("https://3p7zu95yg3.execute-api.us-east-1.amazonaws.com/default/priceFeed2?ticker=" + ticker + "&num_days=" + numberOfDays,
                            PriceFeed.class);
            if (priceFeed.getTicker() != null && priceFeed.getTicker().equals(ticker)) {
                return new ResponseEntity<PriceFeed>(priceFeed, HttpStatus.OK);
            }
        }catch(NoSuchElementException | UnknownContentTypeException | HttpServerErrorException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/getTradeAdvisor/{ticker}")
    public ResponseEntity<TradeAdvisor> getTradeAdvisor(@PathVariable String ticker) {

        try{
            TradeAdvisor tradeAdvisor = restTemplate
                    .getForObject("https://qz4sxjl623.execute-api.us-east-1.amazonaws.com/default/tradeAdvisor?ticker=" + ticker,
                            TradeAdvisor.class);
            if (tradeAdvisor.getTicker() != null && tradeAdvisor.getTicker().equals(ticker)) {
                return new ResponseEntity<TradeAdvisor>(tradeAdvisor, HttpStatus.OK);
            }
        }catch(NoSuchElementException | UnknownContentTypeException | HttpServerErrorException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
