package com.example.hackaton2.controller;

public enum RequestValidity {
    VALID,
    BUYORSELL_ERR,
    EXCEEDSBALANCE,
    PRICE_ERR,
    VOLUME_ERR,
    STOCKTICKER_ERR,
    TIMESTAMP_ERR
}
