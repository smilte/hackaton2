package com.example.hackaton2.controller;

public enum Action {
    BUY("BUY"),
    SELL("SELL");

    private String symbol;

    private Action(String symbol){
        this.symbol = symbol;
    }
}
