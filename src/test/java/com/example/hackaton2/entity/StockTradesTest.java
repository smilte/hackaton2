package com.example.hackaton2.entity;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StockTradesTest {

    private static StockTrades stockTrades;

    @BeforeAll
    public static void setup() {
        stockTrades = new StockTrades();
    }

    @Test
    public void testSetStockTicker() {
        stockTrades.setStockTicker("AMZN");
        assertEquals("AMZN", stockTrades.getStockTicker());
    }

    @Test
    public void testSetPrice() {
        stockTrades.setPrice(352.63);
        assertEquals(352.63, stockTrades.getPrice());
    }
}
