package com.example.hackaton2.controller;


import com.example.hackaton2.entity.StockTrades;
import com.example.hackaton2.service.StockTradeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
public class StockTradesControllerTest {

    @Autowired
    private MockMvc mockMvc;
    private StockTrades stockTrades;
    private StockTradesController stockTradesController;

    @BeforeEach
    public void setup() {
        stockTrades = new StockTrades();
        long id = 1;
        stockTrades.setId(id);
        stockTrades.setStockTicker("AMZN");
        stockTrades.setPrice(3123.12);
        stockTrades.setVolume(1000);
        stockTrades.setBuyOrSell(Action.BUY.toString());
        stockTrades.setStatusCode(0);
        stockTradesController = new StockTradesController();
    }

    @Test
    public void stockTradesControllerFindAllSuccess()throws Exception{
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/stocktrades"))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void stockTradesControllerFindAllNotFound() throws Exception{
        MvcResult mvcResult = this.mockMvc.perform(get("/api/v1/stocktrade"))
                .andExpect(status().isNotFound())
                .andReturn();
    }

    @Test
    public void testFindByIdNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/stocktrades/{id}", 9999)
                .accept(MediaType.APPLICATION_JSON))
                .andDo((print()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSaveSuccess() throws Exception {
        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());
    }


    @Test
    public void testSaveInvalidBuyOrSell() throws Exception {
        stockTrades.setBuyOrSell("INVALID");
        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testSaveAndFindByIdSuccess() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        mockMvc.perform(get("/api/v1/stocktrades/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1));
    }

    @Test
    public void testFindByTickerNotFound() throws Exception {
        mockMvc.perform(get("/api/v1/stocktrades/findByStockTicker/{stockTicker}", "ABC")
                .accept(MediaType.APPLICATION_JSON))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.stockTicker").doesNotExist());
    }

    @Test
    public void testSaveAndFindByTickerSuccess() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        mockMvc.perform(get("/api/v1/stocktrades/findByStockTicker/{stockTicker}", "AMZN")
                .accept(MediaType.APPLICATION_JSON))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[*].stockTicker").value("AMZN"));
    }

    @Test
    public void testFindByStatusCodeNotValid() throws Exception {
        mockMvc.perform(get("/api/v1/stocktrades/findByStatusCode/{statusCode}", "100")
                .accept(MediaType.APPLICATION_JSON))
                .andDo((print()))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testSavePriceValidationFailure() throws Exception {

        stockTrades.setPrice(-1);
        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testSaveVolumeValidationFailure() throws Exception {

        stockTrades.setVolume(-1);
        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable());

    }

    @Test
    public void testUpdateSuccess() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        stockTrades.setPrice(3444.44);
        mockMvc.perform(put("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/v1/stocktrades/{id}", 1)
                .accept(MediaType.APPLICATION_JSON))
                .andDo((print()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.price").value(3444.44));
    }

    @Test
    public void testUpdateInvalidPrice() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        stockTrades.setPrice(-1);
        mockMvc.perform(put("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable());
    }
    @Test
    public void testUpdateInvalidBuyOrSell() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        stockTrades.setBuyOrSell("NONE");
        mockMvc.perform(put("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testUpdateInvalidVolume() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        stockTrades.setVolume(-1);
        mockMvc.perform(put("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testUpdateInvalidTicker() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        stockTrades.setStockTicker("");
        mockMvc.perform(put("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testUpdateInvalidTimestamp() throws Exception {

        mockMvc.perform(post("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
        stockTrades.setCreatedTimestamp(null);
        mockMvc.perform(put("/api/v1/stocktrades")
                .content(asJsonString(stockTrades))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    public void testCheckRequestValidityPriceError() {

        stockTrades.setPrice(-1.0);
        assertEquals(RequestValidity.PRICE_ERR, stockTradesController.checkRequestValidity(stockTrades));
    }

    @Test
    public void testCheckRequestValidityBuyOrSellError() {

        stockTrades.setBuyOrSell("NONE");
        assertEquals(RequestValidity.BUYORSELL_ERR, stockTradesController.checkRequestValidity(stockTrades));
    }

    @Test
    public void testCheckRequestValidityVolumeError() {

        stockTrades.setVolume(-10);
        assertEquals(RequestValidity.VOLUME_ERR, stockTradesController.checkRequestValidity(stockTrades));
    }

    @Test
    public void testCheckRequestValidityStockTickerError() {

        stockTrades.setStockTicker("");
        assertEquals(RequestValidity.STOCKTICKER_ERR, stockTradesController.checkRequestValidity(stockTrades));
    }

    @Test
    public void testCheckRequestValidityTimestampError() {

        stockTrades.setCreatedTimestamp(null);
        assertEquals(RequestValidity.TIMESTAMP_ERR, stockTradesController.checkRequestValidity(stockTrades));
    }

    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
