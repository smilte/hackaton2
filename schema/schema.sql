CREATE Database stocktrades;

USE stocktrades;

CREATE TABLE IF NOT EXISTS stocktrades (
                               id bigint NOT NULL AUTO_INCREMENT,
                               buyOrSell varchar(255) DEFAULT NULL,
                               price double NOT NULL,
                               statusCode int NOT NULL,
                               stockTicker varchar(255) DEFAULT NULL,
                               volume int NOT NULL,
                               createdTimestamp datetime DEFAULT NULL,
                               PRIMARY KEY (id)
);
